﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Multiples_3or5
{
    /// <summary>
    /// Purpose of program is to calculate the multiples of 3 and 5 and calculate the SUM.
    /// 24/09/2016
    /// Rebecca Knight
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_1_Click(object sender, RoutedEventArgs e)
        {

            int numberFive = 0;
            // Loop through all integers below 1000.
            for (int i = 1; i < 1000; i++)
            {
                // If 1000/5 leaves no remainder then add that value to the variable numberFive.
                if ((i % 5) == 0)
                {
                    numberFive += i;
                }
            }

            // Repeating the process for integer 3
            int numberThree = 0;
            for (int i = 1; i < 1000; i++)
            {
                if ((i % 3) == 0)
                {
                    numberThree += i;
                }
            }


            txt_bx_1.Text = (numberThree + numberFive).ToString();

        }
    }
}
